import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "MuelllasterTonnen")

public class MuelllasterTonnen {

	@DatabaseField(id = true)
	private String modellNR;
	
	@DatabaseField(canBeNull = false)
	private int muelltonnenID;
	
	
	public MuelllasterTonnen() {}

	public MuelllasterTonnen(String modellNR, int muelltonnenID) {
		super();
		this.modellNR = modellNR;
		this.muelltonnenID = muelltonnenID;
	}

	public String getModellNR() {
		return modellNR;
	}

	public void setModellNR(String modellNR) {
		this.modellNR = modellNR;
	}

	public int getMuelltonnenID() {
		return muelltonnenID;
	}

	public void setMuelltonnenID(int muelltonnenID) {
		this.muelltonnenID = muelltonnenID;
	}

	
}
