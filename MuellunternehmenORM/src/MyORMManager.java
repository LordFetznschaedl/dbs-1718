import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class MyORMManager {
	
	
	private final static String DATABASE_URL = "jdbc:mysql://localhost:3306/muellunternehmenorm?useSSL=false";
	
	private Dao<Hersteller, Integer> herstellerDao;
	
	private Dao<Muelltonne, Integer> muelltonneDao;
	
	private Dao<Muelllaster, Integer> muelllasterDao;
	
	private Dao<MuelllasterTonnen, Integer> muelllasterTonnenDao;
	
	private Dao<Orte, Integer> orteDao;
	
	private Dao<Tagesplan, Integer> tagesplanDao;
	
	

	public static void main(String[] args) throws Exception {
		
		new MyORMManager().doMain(args);
	}
	
	public void doMain(String[] args) throws Exception 
	{
		ConnectionSource connectionSource = null;
		try {
			
			connectionSource = new JdbcConnectionSource(DATABASE_URL,"test","test");
			
			setupDatabase(connectionSource);
			readWriteData();
			
			System.out.println("\n\nIt seems to have worked\n\n");
		} 
		finally 
		{
			if (connectionSource != null) 
			{
				connectionSource.close();
			}
		}

	}
	
	private void setupDatabase(ConnectionSource connectionSource) throws Exception {

		herstellerDao = DaoManager.createDao(connectionSource, Hersteller.class);
		TableUtils.dropTable(connectionSource, Hersteller.class, false);
		TableUtils.createTable(connectionSource, Hersteller.class);
		
		
		muelltonneDao = DaoManager.createDao(connectionSource, Muelltonne.class);
		TableUtils.dropTable(connectionSource, Muelltonne.class, false);
		TableUtils.createTable(connectionSource, Muelltonne.class);
		
		muelllasterDao = DaoManager.createDao(connectionSource, Muelllaster.class);
		TableUtils.dropTable(connectionSource, Muelllaster.class, false);
		TableUtils.createTable(connectionSource, Muelllaster.class);
		
		muelllasterTonnenDao = DaoManager.createDao(connectionSource, MuelllasterTonnen.class);
		TableUtils.dropTable(connectionSource, MuelllasterTonnen.class, false);
		TableUtils.createTable(connectionSource, MuelllasterTonnen.class);
		
		orteDao = DaoManager.createDao(connectionSource, Orte.class);
		TableUtils.dropTable(connectionSource, Orte.class, false);
		TableUtils.createTable(connectionSource, Orte.class);
		
		tagesplanDao = DaoManager.createDao(connectionSource, Tagesplan.class);
		TableUtils.dropTable(connectionSource, Tagesplan.class, false);
		TableUtils.createTable(connectionSource, Tagesplan.class);
	}

	
	private void readWriteData() throws Exception {
		
		Hersteller hersteller = new Hersteller(1 , 6020 , "Innsbruck" , "ABC-Strasse" , "123a" , "Peter");
		herstellerDao.create(hersteller);
		
		Muelltonne muelltonne = new Muelltonne(1 , 1 , 55.55);
		muelltonneDao.create(muelltonne);
		
		Muelllaster muelllaster = new Muelllaster(1 , 1 , "ABC123", "I-1234AB");
		muelllasterDao.create(muelllaster);
		
		MuelllasterTonnen muelllasterTonnen = new MuelllasterTonnen("ABC123" , 1);
		muelllasterTonnenDao.create(muelllasterTonnen);
		
		Orte orte = new Orte(1 , 6020 , "Innsbruck" , 15);
		orteDao.create(orte);
		
		Tagesplan tagesplan = new Tagesplan(1 , 2 , 5 ,  "2017-05-05 12:12:00");
		tagesplanDao.create(tagesplan);
	
	}
	
	
	
}
