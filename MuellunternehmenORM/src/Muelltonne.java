import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Muelltonne")

public class Muelltonne {

	@DatabaseField(id = true)
	private int muelltonnenID; 
	
	@DatabaseField(canBeNull = false)
	private int herstellerID;
	
	@DatabaseField(canBeNull = false)
	private double fassungsvermoegen;
	
	
	public Muelltonne() {}

	public Muelltonne(int muelltonnenID, int herstellerID, double fassungsvermoegen) {
		super();
		this.muelltonnenID = muelltonnenID;
		this.herstellerID = herstellerID;
		this.fassungsvermoegen = fassungsvermoegen;
	}

	public int getMuelltonnenID() {
		return muelltonnenID;
	}

	public void setMuelltonnenID(int muelltonnenID) {
		this.muelltonnenID = muelltonnenID;
	}

	public int getHerstellerID() {
		return herstellerID;
	}

	public void setHerstellerID(int herstellerID) {
		this.herstellerID = herstellerID;
	}

	public double getFassungsvermoegen() {
		return fassungsvermoegen;
	}

	public void setFassungsvermoegen(double fassungsvermoegen) {
		this.fassungsvermoegen = fassungsvermoegen;
	}

	
}
