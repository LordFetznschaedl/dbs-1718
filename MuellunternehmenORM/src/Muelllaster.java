import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Muelllaster")

public class Muelllaster {

	@DatabaseField(id = true)
	private int muelllasterID;
	
	@DatabaseField(canBeNull = false)
	private int herstellerID;
	
	@DatabaseField(canBeNull = false)
	private String modellNR;
	
	@DatabaseField(canBeNull = false)
	private String kennzeichen;
	
	
	public Muelllaster() {}

	public Muelllaster(int muelllasterID, int herstellerID, String modellNR, String kennzeichen) {
		super();
		this.muelllasterID = muelllasterID;
		this.herstellerID = herstellerID;
		this.modellNR = modellNR;
		this.kennzeichen = kennzeichen;
	}

	public int getMuelllasterID() {
		return muelllasterID;
	}

	public void setMuelllasterID(int muelllasterID) {
		this.muelllasterID = muelllasterID;
	}

	public int getHerstellerID() {
		return herstellerID;
	}

	public void setHerstellerID(int herstellerID) {
		this.herstellerID = herstellerID;
	}

	public String getModellNR() {
		return modellNR;
	}

	public void setModellNR(String modellNR) {
		this.modellNR = modellNR;
	}

	public String getKennzeichen() {
		return kennzeichen;
	}

	public void setKennzeichen(String kennzeichen) {
		this.kennzeichen = kennzeichen;
	}

	
}
