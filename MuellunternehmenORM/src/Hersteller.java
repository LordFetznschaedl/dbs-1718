import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Hersteller")

public class Hersteller {
	
	@DatabaseField(id = true)
	private int herstellerID;
	
	@DatabaseField(canBeNull = false)
	private int postleitzahl;
	
	@DatabaseField(canBeNull = false)
	private String ort;
	
	@DatabaseField(canBeNull = false)
	private String adresse;
	
	@DatabaseField(canBeNull = false)
	private String hausnummer;
	
	@DatabaseField(canBeNull = false)
	private String ansprechperson; 
	
	
	public Hersteller() {}

	public Hersteller(int herstellerID, int postleitzahl, String ort, String adresse, String hausnummer, String ansprechperson) {
		super();
		this.herstellerID = herstellerID;
		this.postleitzahl = postleitzahl;
		this.ort = ort;
		this.adresse = adresse;
		this.hausnummer = hausnummer;
		this.ansprechperson = ansprechperson;
	}

	public int getHerstellerID() {
		return herstellerID;
	}

	public void setHerstellerID(int herstellerID) {
		this.herstellerID = herstellerID;
	}

	public int getPostleitzahl() {
		return postleitzahl;
	}

	public void setPostleitzahl(int postleitzahl) {
		this.postleitzahl = postleitzahl;
	}

	public String getOrt() {
		return ort;
	}

	public void setOrt(String ort) {
		this.ort = ort;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getHausnummer() {
		return hausnummer;
	}

	public void setHausnummer(String hausnummer) {
		this.hausnummer = hausnummer;
	}

	public String getAnsprechperson() {
		return ansprechperson;
	}

	public void setAnsprechperson(String ansprechperson) {
		this.ansprechperson = ansprechperson;
	}

	
}
