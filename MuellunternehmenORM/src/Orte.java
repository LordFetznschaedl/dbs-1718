import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Orte")

public class Orte {

	@DatabaseField(id = true)
	private int ortID;
	
	@DatabaseField(canBeNull = false)
	private int postleitzahl;
	
	@DatabaseField(canBeNull = false)
	private String ortsname;
	
	@DatabaseField(canBeNull = false)
	private int anzahlStadtbereiche;
	
	
	public Orte() {}

	public Orte(int ortID, int postleitzahl, String ortsname, int anzahlStadtbereiche) {
		super();
		this.ortID = ortID;
		this.postleitzahl = postleitzahl;
		this.ortsname = ortsname;
		this.anzahlStadtbereiche = anzahlStadtbereiche;
	}

	public int getOrtID() {
		return ortID;
	}

	public void setOrtID(int ortID) {
		this.ortID = ortID;
	}

	public int getPostleitzahl() {
		return postleitzahl;
	}

	public void setPostleitzahl(int postleitzahl) {
		this.postleitzahl = postleitzahl;
	}

	public String getOrtsname() {
		return ortsname;
	}

	public void setOrtsname(String ortsname) {
		this.ortsname = ortsname;
	}

	public int getAnzahlStadtbereiche() {
		return anzahlStadtbereiche;
	}

	public void setAnzahlStadtbereiche(int anzahlStadtbereiche) {
		this.anzahlStadtbereiche = anzahlStadtbereiche;
	}

	
}
