import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Tagesplan")

public class Tagesplan {

	@DatabaseField(canBeNull = false)
	private int muelllasterID;
	
	@DatabaseField(canBeNull = false)
	private int ortID;
	
	@DatabaseField(canBeNull = false)
	private int ortsbereich; 
	
	@DatabaseField(canBeNull = false)
	private String datum;
	
	
	public Tagesplan() {}

	public Tagesplan(int muelllasterID, int ortID, int ortsbereich, String datum) {
		super();
		this.muelllasterID = muelllasterID;
		this.ortID = ortID;
		this.ortsbereich = ortsbereich;
		this.datum = datum;
	}

	public int getMuelllasterID() {
		return muelllasterID;
	}

	public void setMuelllasterID(int muelllasterID) {
		this.muelllasterID = muelllasterID;
	}

	public int getOrtID() {
		return ortID;
	}

	public void setOrtID(int ortID) {
		this.ortID = ortID;
	}

	public int getOrtsbereich() {
		return ortsbereich;
	}

	public void setOrtsbereich(int ortsbereich) {
		this.ortsbereich = ortsbereich;
	}

	public String getDatum() {
		return datum;
	}

	public void setDatum(String datum) {
		this.datum = datum;
	}

	
}
