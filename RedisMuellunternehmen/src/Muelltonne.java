import java.util.List;

import redis.clients.jedis.Jedis;

public class Muelltonne {

private static Muelltonne instance;
	
	private Muelltonne(){}
	
	public static Muelltonne getInstance()
	{
		if(Muelltonne.instance==null)
		{
			Muelltonne.instance=new Muelltonne();
		}
		return Muelltonne.instance;
	}
	
	public void insertMuelltonne(Jedis jedis, int id, int fassungsvermögen) {
		jedis.rpush("muelltonne", id+"", fassungsvermögen+"");
		System.out.println("Inserted Data");
	}
	
	public void readMuelltonne(Jedis jedis)
	{
		List<String> list = jedis.lrange("muelltonne", 0 , -1);
		
		for(int i = 0; i<list.size(); i=i+2) { 
	        System.out.println("Stored string in redis-muelltonne: id: "+list.get(i)+", fassungsvermögen: "+list.get(i+1)); 
	    } 
	}
}

