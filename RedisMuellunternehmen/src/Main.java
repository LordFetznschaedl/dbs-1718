import redis.clients.jedis.Jedis;

public class Main {

	public static void main(String[] args) {
		
		Jedis jedis = new Jedis("localhost"); 
		System.out.println("Connection to server sucessfully"); 
		System.out.println(jedis.ping());
		
		jedis.del("muelllaster");
		jedis.del("muelltonne");
		
		Muelllaster laster = Muelllaster.getInstance();
		Muelltonne tonne = Muelltonne.getInstance();
		
		laster.getInstance().insertMuelllaster(jedis, 1, 12, "I-0815");
		laster.getInstance().insertMuelllaster(jedis, 2, 14, "IL-0816");
		
		tonne.getInstance().insertMuelltonne(jedis, 1, 160);
		tonne.getInstance().insertMuelltonne(jedis, 2, 240);
		
		laster.getInstance().readMuelllaster(jedis);
		tonne.getInstance().readMuelltonne(jedis);
	}

}
