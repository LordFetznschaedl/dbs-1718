import java.util.List;

import redis.clients.jedis.Jedis;

public class Muelllaster {
	
	private static Muelllaster instance;
	
	private Muelllaster(){}
	
	public static Muelllaster getInstance()
	{
		if(Muelllaster.instance==null)
		{
			Muelllaster.instance=new Muelllaster();
		}
		return Muelllaster.instance;
	}
	
	public void insertMuelllaster(Jedis jedis, int id, int modellNr , String kennzeichen) {
		jedis.rpush("muelllaster", id+"", modellNr+"", kennzeichen);
		System.out.println("Inserted Data");
	}
	
	public void readMuelllaster(Jedis jedis)
	{
		List<String> list = jedis.lrange("muelllaster", 0 , -1);
		
		for(int i = 0; i<list.size(); i=i+3) { 
	        System.out.println("Stored string in redis-muelllaster: id: "+list.get(i)+", modellNr: "+list.get(i+1)+", kennzeichen: "+list.get(i+2)); 
	    } 
	}
}
