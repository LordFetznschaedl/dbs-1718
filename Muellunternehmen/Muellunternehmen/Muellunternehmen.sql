
-- -----------------------------------------------------
-- Table Muellentsorgung.Hersteller
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Muellentsorgung.Hersteller (
  herstellerID INT NOT NULL,
  postleitzahl INT NOT NULL,
  ort VARCHAR(45) NOT NULL,
  adresse VARCHAR(45) NOT NULL,
  hausnummer VARCHAR(45) NOT NULL,
  ansprechperson VARCHAR(45) NOT NULL,
  PRIMARY KEY (herstellerID))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table Muellentsorgung.Muelltonne
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Muellentsorgung.Muelltonne (
  muelltonnenID INT NOT NULL,
  herstellerID INT NOT NULL,
  fassungsvermoegen REAL NOT NULL,
  PRIMARY KEY (muelltonnenID))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table Muellentsorgung.Muelllaster
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Muellentsorgung.Muelllaster (
  muelllasterID INT NOT NULL,
  herstellerID INT NOT NULL,
  modellNR VARCHAR(45) NOT NULL,
  kennzeichen VARCHAR(45) NOT NULL,
  PRIMARY KEY (muelllasterID, modellNR))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table Muellentsorgung.MuelllasterTonnen
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Muellentsorgung.MuelllasterTonnen (
  modellNR VARCHAR(45) NOT NULL,
  muelltonnenID INT NOT NULL,
  PRIMARY KEY (modellNR, muelltonnenID))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table Muellentsorgung.Orte
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Muellentsorgung.Orte (
  ortID INT NOT NULL,
  postleitzahl INT NOT NULL,
  ortsname VARCHAR(45) NOT NULL,
  anzahlStadtbereiche INT NOT NULL,
  PRIMARY KEY (ortID))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table Muellentsorgung.Tagesplan
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Muellentsorgung.Tagesplan (
  muelllasterID INT NOT NULL,
  ortID INT NOT NULL,
  ortsbereich INT NOT NULL,
  datum DATETIME NOT NULL)
ENGINE = InnoDB;

