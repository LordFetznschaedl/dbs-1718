import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextPane;

public class GUI extends JFrame{

	public static JPanel muelllasterPanel = new JPanel();
	public static JPanel muelltonnePanel = new JPanel();
	public static JPanel herstellerPanel = new JPanel();
	public static JPanel ortPanel = new JPanel();
	public static JPanel tagesplanPanel = new JPanel();
	public static JPanel muelltonneMuelllasterPanel = new JPanel();
	
	public static JComboBox muelllasterIDTagesplan = new JComboBox();
	public static JComboBox ortIDTagesplan = new JComboBox();
	public static JComboBox ortsbereichTagesplan = new JComboBox();
	public static JComboBox  jahrTagesplan = new JComboBox();
	public static JComboBox  monatTagesplan = new JComboBox();
	public static JComboBox  tagTagesplan = new JComboBox();
	public static JComboBox  stundeTagesplan = new JComboBox();
	public static JComboBox  minuteTagesplan = new JComboBox();
	public static JButton hinzufuegenTagesplan = new JButton("Add to Database!");
	public static JLabel muelllasterIDTextTagesplan = new JLabel("ID des Muelllaster:");
	public static JLabel ortIDTextTagesplan = new JLabel("ID des Orts:");
	public static JLabel ortsbereichTextTagesplan = new JLabel("Ortsbereich:");
	public static JLabel jahrTextTagesplan = new JLabel("Jahr:");
	public static JLabel monatTextTagesplan = new JLabel("Monat:");
	public static JLabel tagTextTagesplan = new JLabel("Tag:");
	public static JLabel stundeTextTagesplan = new JLabel("Stunde:");
	public static JLabel minuteTextTagesplan = new JLabel("Minute:");
	
	public static JTextField pathMuelltonne = new JTextField("");
	public static JTextField pathMuelllaster = new JTextField("");
	public static JTextField pathHersteller = new JTextField("");
	public static JTextField pathOrt = new JTextField("");
	
	public static JLabel pathMuelltonneText = new JLabel("Path: ");
	public static JLabel pathMuelllasterText = new JLabel("Path: ");
	public static JLabel pathOrtText = new JLabel("Path: ");
	public static JLabel pathHerstellerText = new JLabel("Path: ");
					
	
	public static JButton buttonMuelltonne = new JButton("hinzufügen");
	public static JButton buttonMuelllaster = new JButton("hinzufügen");
	public static JButton buttonOrt = new JButton("hinzufügen");
	public static JButton buttonHersteller = new JButton("hinzufügen");
	
	public static JMenuBar menubar = new JMenuBar();
	public static JMenuItem menuMuelllaster = new JMenuItem("Muelllaster");
	public static JMenuItem menuMuelltonne = new JMenuItem("Muelltonne");
	public static JMenuItem menuHersteller = new JMenuItem("Hersteller");
	public static JMenuItem menuOrt = new JMenuItem("Ort");
	public static JMenuItem menuTagesplan = new JMenuItem("Tagesplan");
	public static JMenuItem menuMuelltonneMuelllaster = new JMenuItem("Muelllaster Zuordnung Muelltonne");

	public static Statement stmt=null;
	public static ResultSet rs = null;
	public static ResultSet rsMuelllasterID = null;
	public static ResultSet rsOrtID = null;
	public static ResultSet rsOrtsereiche = null;
	
	public static JPanel lastPanel;
	
	public static JLabel muelllasterText = new JLabel("Muelllaster");
	public static JLabel herstellerText = new JLabel("Hersteller");
	public static JLabel ortText = new JLabel("Ort");
	
	public static Calendar cal = new GregorianCalendar();
	
	public static int jahrI;
	public static int monatI;
	public static int tagI;
	public static int stundeI;
	public static int minuteI;

	public GUI()
	{

		this.setTitle("Müllentsorgung");
		this.setLayout(new BorderLayout());
		this.setPreferredSize(new Dimension(800, 200));
		this.setLocationRelativeTo(null);

		ActionListener ortsListner = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				fillOrtsbereiche();
			}
		};
		
		ActionListener hinzufuegenListner = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				hinzufuegen();
			}
		};
		
		ActionListener datumListner = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				fillTag();
			}
		};
		
		ActionListener menuListner = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				menu(ae);
			}
		};
		
		ActionListener buttonHinzufuegenListner = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				String acS = ae.getActionCommand();
				if(acS.equals("Orte"))
				{
					Main.readInsertFile(pathOrt.getText(), "Orte");
					readDatabase("Orte");
				}
				else if(acS.equals("Hersteller"))
				{
					Main.readInsertFile(pathHersteller.getText(), "Hersteller");
					readDatabase("Hersteller");
				}
				else if(acS.equals("Muelllaster"))
				{
					Main.readInsertFile(pathMuelllaster.getText(), "Muelllaster");
					readDatabase("Muelllaster");
				}
				else if(acS.equals("Muelltonne"))
				{
					Main.readInsertFile(pathMuelltonne.getText(), "Muelltonne");
					readDatabase("Muelltonne");
				}
			}
		};
		

		tagesplanPanel.add(muelllasterIDTextTagesplan);
		tagesplanPanel.add(muelllasterIDTagesplan);
		tagesplanPanel.add(ortIDTextTagesplan);
		tagesplanPanel.add(ortIDTagesplan);
		tagesplanPanel.add(ortsbereichTextTagesplan);
		tagesplanPanel.add(ortsbereichTagesplan);
		tagesplanPanel.add(jahrTextTagesplan);
		tagesplanPanel.add(jahrTagesplan);
		tagesplanPanel.add(monatTextTagesplan);
		tagesplanPanel.add(monatTagesplan);
		tagesplanPanel.add(tagTextTagesplan);
		tagesplanPanel.add(tagTagesplan);
		tagesplanPanel.add(stundeTextTagesplan);
		tagesplanPanel.add(stundeTagesplan);
		tagesplanPanel.add(minuteTextTagesplan);
		tagesplanPanel.add(minuteTagesplan);
		tagesplanPanel.add(hinzufuegenTagesplan);
		
		muelltonnePanel.add(pathMuelltonneText);
		muelllasterPanel.add(pathMuelllasterText);
		ortPanel.add(pathOrtText);
		herstellerPanel.add(pathHerstellerText);
		
		muelltonnePanel.add(pathMuelltonne);
		muelllasterPanel.add(pathMuelllaster);
		ortPanel.add(pathOrt);
		herstellerPanel.add(pathHersteller);

		muelltonnePanel.add(buttonMuelltonne);
		muelllasterPanel.add(buttonMuelllaster);
		ortPanel.add(buttonOrt);
		herstellerPanel.add(buttonHersteller);
		
		pathMuelllaster.setPreferredSize( new Dimension( 500, 24 ) );
		pathMuelltonne.setPreferredSize( new Dimension( 500, 24 ) );
		pathHersteller.setPreferredSize( new Dimension( 500, 24 ) );
		pathOrt.setPreferredSize( new Dimension( 500, 24 ) );
		
		muelllasterIDTagesplan.setEditable(false);
		ortIDTagesplan.setEditable(false);
		ortsbereichTagesplan.setEditable(false);

		menubar.add(menuMuelllaster);
		menubar.add(menuMuelltonne);
		menubar.add(menuHersteller);
		menubar.add(menuOrt);
		menubar.add(menuTagesplan);
		menubar.add(menuMuelltonneMuelllaster);
		
		this.setJMenuBar(menubar);

		this.add(tagesplanPanel);

		fillMuelllasterID();
		fillOrtID();
		fillOrtsbereiche();
		fillZeiten();
		fillTag();

		ortIDTagesplan.addActionListener(ortsListner);
		hinzufuegenTagesplan.addActionListener(hinzufuegenListner);
		monatTagesplan.addActionListener(datumListner);
		
		menuMuelllaster.addActionListener(menuListner);
		menuMuelltonne.addActionListener(menuListner);
		menuHersteller.addActionListener(menuListner);
		menuOrt.addActionListener(menuListner);
		menuTagesplan.addActionListener(menuListner);
		menuMuelltonneMuelllaster.addActionListener(menuListner);
		
		menuMuelllaster.setActionCommand("Muelllaster");
		menuMuelltonne.setActionCommand("Muelltonne");
		menuHersteller.setActionCommand("Hersteller");
		menuOrt.setActionCommand("Ort");
		menuTagesplan.setActionCommand("Tagesplan");
		menuMuelltonneMuelllaster.setActionCommand("MuelllasterTonne");
		
		buttonMuelllaster.setActionCommand("Muelllaster");
		buttonMuelltonne.setActionCommand("Muelltonne");
		buttonHersteller.setActionCommand("Hersteller");
		buttonOrt.setActionCommand("Orte");
		
		buttonMuelltonne.addActionListener(buttonHinzufuegenListner);
		buttonMuelllaster.addActionListener(buttonHinzufuegenListner);
		buttonOrt.addActionListener(buttonHinzufuegenListner);
		buttonHersteller.addActionListener(buttonHinzufuegenListner);
		
		lastPanel=tagesplanPanel;
		
		init();

	}

	public void init()
	{
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.pack();
		this.setVisible(true);
	}


	public static void fillMuelllasterID()
	{


		try 
		{
			stmt=Main.c.createStatement();
			rsMuelllasterID=stmt.executeQuery("select muelllasterID from Muelllaster;");
			int columns = rsMuelllasterID.getMetaData().getColumnCount();

			while (rsMuelllasterID.next())
			{
				for (int i = 1; i <= columns; i++) 
				{
					System.out.print(rsMuelllasterID.getString(i) + "\t\t");
					muelllasterIDTagesplan.addItem(rsMuelllasterID.getString(i));
				}
				System.out.println("");
			}
			rsMuelllasterID.close();
			stmt.close();
		} catch (SQLException e) {

			e.printStackTrace();
		}

	}

	public static void fillOrtID()
	{


		try 
		{
			stmt=Main.c.createStatement();
			rsOrtID=stmt.executeQuery("select ortID from Orte;");
			int columns = rsOrtID.getMetaData().getColumnCount();
			while (rsOrtID.next())
			{
				for (int i = 1; i <= columns; i++) 
				{
					System.out.print(rsOrtID.getString(i) + "\t\t");
					ortIDTagesplan.addItem(rsOrtID.getString(i));
				}
				System.out.println("");
			}
			rsOrtID.close();
			stmt.close();
		} 
		catch (SQLException e) 
		{

			e.printStackTrace();
		}

	}
	
	
	public static void fillOrtsbereiche()
	{
		try 
		{

			ortsbereichTagesplan.removeAllItems();

			stmt=Main.c.createStatement();
			rsOrtsereiche=stmt.executeQuery("select anzahlStadtbereiche from Orte where ortID = "+ortIDTagesplan.getSelectedItem()+";");
			int columns = rsOrtsereiche.getMetaData().getColumnCount();
			int bereichsanzahl=0;
			while (rsOrtsereiche.next())
			{
				for (int i = 1; i <= columns; i++) 
				{
					System.out.print(rsOrtsereiche.getString(i) + "\t\t");
					bereichsanzahl=Integer.parseInt(rsOrtsereiche.getString(i));
				}
				System.out.println("");
			}

			for(int i=1;i<=bereichsanzahl;i++)
			{
				ortsbereichTagesplan.addItem(i);
			}
			rsOrtsereiche.close();
			stmt.close();
		} 
		catch (SQLException e) 
		{

			e.printStackTrace();
		}
	}
	
	public static void getZeiten()
	{
		jahrI=cal.get(Calendar.YEAR);
		monatI=cal.get(Calendar.MONTH);
		tagI=cal.get(Calendar.DAY_OF_MONTH);
		stundeI=cal.get(Calendar.HOUR_OF_DAY);
		minuteI=cal.get(Calendar.MINUTE);
	}
	
	public static void fillZeiten()
	{
		getZeiten();
		
		for(int i=jahrI;i<=jahrI+10;i++)
		{
			jahrTagesplan.addItem(i);
			jahrTagesplan.setSelectedItem(jahrI);
		}
		for(int i=1;i<=12;i++)
		{
			monatTagesplan.addItem(i);
			monatTagesplan.setSelectedItem(monatI);
		}
		for(int i=0;i<=23;i++)
		{
			stundeTagesplan.addItem(i);
			stundeTagesplan.setSelectedItem(stundeI);
		}
		for(int i=0;i<=59;i++)
		{
			minuteTagesplan.addItem(i);
			minuteTagesplan.setSelectedItem(minuteI);
		}
	}
	
	public static void fillTag()
	{
		tagTagesplan.removeAllItems();
		getZeiten();
		int monatIS=(int) monatTagesplan.getSelectedItem();
		
		if(monatIS==1||monatIS==3||monatIS==5||monatIS==7||monatIS==8||monatIS==10||monatIS==12)
		{
			for(int i=1;i<=31;i++)
			{
				tagTagesplan.addItem(i);
			}
		}
		else if(monatIS==2)
		{
			for(int i=1;i<=28;i++)
			{
				tagTagesplan.addItem(i);
			}
		}
		else
		{
			for(int i=1;i<=30;i++)
			{
				tagTagesplan.addItem(i);
			}
		}
		
		
		
		if(monatI+1==monatIS)
		{
			tagTagesplan.setSelectedIndex(tagI-1);
		}
		else
		{
			tagTagesplan.setSelectedItem(0);
		}
		
	}
	
	public static void hinzufuegen()
	{
		String datum = jahrTagesplan.getSelectedItem().toString() +"-"+ monatTagesplan.getSelectedItem().toString() +"-"+ tagTagesplan.getSelectedItem().toString() +" "+ stundeTagesplan.getSelectedItem().toString() +":"+minuteTagesplan.getSelectedItem().toString() +":00";
		String values = muelllasterIDTagesplan.getSelectedItem().toString()+","+ortIDTagesplan.getSelectedItem().toString()+","+ortsbereichTagesplan.getSelectedItem().toString()+",\""+datum+"\""; 
		
		Main.useStatement("insert into Tagesplan values("+values+");");
		refresh();
		
		try 
		{
			stmt=Main.c.createStatement();
			rs = stmt.executeQuery("SELECT * FROM Tagesplan");
	        int columns = rs.getMetaData().getColumnCount();
	        
	        System.out.println();
	        
	        for (int i = 1; i <= columns; i++)
	        {
	            System.out.print(rs.getMetaData().getColumnLabel(i) + "\t\t");
	        }
	        System.out.println();
	        System.out.println();
	      
	        while (rs.next()) 
	        {
	            for (int i = 1; i <= columns; i++) 
	            {
	                System.out.print(rs.getString(i) + "\t\t");
	            }
	            System.out.println();
	        }
	        
	        rs.close();
	        stmt.close();
		} 
		catch (SQLException e) 
		{
			
			e.printStackTrace();
		}
        
	}
	
	public static void refresh()
	{
		
		getZeiten();
		
		muelllasterIDTagesplan.removeAllItems();
		fillMuelllasterID();
		
		ortIDTagesplan.removeAllItems();
		fillOrtID();
		
		ortsbereichTagesplan.removeAllItems();
		fillOrtsbereiche();
		
		jahrTagesplan.setSelectedItem(jahrI);
		monatTagesplan.setSelectedItem(monatI);
		stundeTagesplan.setSelectedItem(stundeI);
		minuteTagesplan.setSelectedItem(minuteI);
	}
	
	public void menu(ActionEvent ae)
	{
		String ac = ae.getActionCommand();
		
		if(ac.equals("Muelllaster"))
		{
			System.out.println(ac);
			
			this.remove(lastPanel);
			lastPanel=muelllasterPanel;
			this.add(muelllasterPanel);
			
		}
		if(ac.equals("Muelltonne"))
		{
			System.out.println(ac);
			
			this.remove(lastPanel);
			lastPanel=muelltonnePanel;
			this.add(muelltonnePanel);
		}
		if(ac.equals("Hersteller"))
		{
			System.out.println(ac);
			
			this.remove(lastPanel);
			lastPanel=herstellerPanel;
			this.add(herstellerPanel);
		}
		if(ac.equals("Ort"))
		{
			System.out.println(ac);
			
			this.remove(lastPanel);
			lastPanel=ortPanel;
			this.add(ortPanel);
			
		}
		if(ac.equals("Tagesplan"))
		{
			System.out.println(ac);
			
			this.remove(lastPanel);
			lastPanel=tagesplanPanel;
			this.add(tagesplanPanel);
			
		}
		if(ac.equals("MuelllasterTonne"))
		{
			System.out.println(ac);
			
			this.remove(lastPanel);
			lastPanel=muelltonneMuelllasterPanel;
			this.add(muelltonneMuelllasterPanel);
			
		}
		this.pack();
		this.repaint();
		
	}
	
	public static void readDatabase(String s)
	{
		try 
		{
			stmt=Main.c.createStatement();
			rs = stmt.executeQuery("SELECT * FROM "+s);
	        int columns = rs.getMetaData().getColumnCount();
	        
	        System.out.println();
	        
	        for (int i = 1; i <= columns; i++)
	        {
	            System.out.print(rs.getMetaData().getColumnLabel(i) + "\t\t");
	        }
	        System.out.println();
	        System.out.println();
	      
	        while (rs.next()) 
	        {
	            for (int i = 1; i <= columns; i++) 
	            {
	                System.out.print(rs.getString(i) + "\t\t");
	            }
	            System.out.println();
	        }
	        
	        rs.close();
	        stmt.close();
		} 
		catch (SQLException e) 
		{
			
			e.printStackTrace();
		}
	}

}
