import java.io.*;
import java.sql.*;



public class Main {

	public static Connection c = null;
	public static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
	public static Statement stmt=null;
	public static ResultSet rs = null;
	public static BufferedReader br;
	public static boolean ziffer;
	public static String buchstaben;
	public static int zahlen;
	public static boolean line;


	public static void main(String[] args) {

		try {

			String myString = "1"  ;
			pruefeString(myString);

			System.out.println("Bitte MySQL Username eingeben: ");
			String username = in.readLine();

			System.out.println("Bitte MySQL Passwort eingeben: ");
			String password = in.readLine();

			


			Class.forName("com.mysql.jdbc.Driver").newInstance();

			
			c = DriverManager.getConnection("jdbc:mysql://localhost?useSSL=false",username,password);
			stmt= c.createStatement();


			stmt.execute("drop database Muellentsorgung;");
			System.out.println("dropped database");

			stmt.execute("create database Muellentsorgung;");
			System.out.println("created database");

			stmt.execute("use Muellentsorgung;");
			System.out.println("connected to database");

			stmt.close();

			readStmtFile("Muellunternehmen/Muellunternehmen.sql");

			readInsertFile("Muellunternehmen/Hersteller.txt","Hersteller");
			readInsertFile("Muellunternehmen/Muelltonne.txt","Muelltonne");
			readInsertFile("Muellunternehmen/Muelllaster.txt","Muelllaster");
			readInsertFile("Muellunternehmen/MuelllasterTonnen.txt","MuelllasterTonnen");
			readInsertFile("Muellunternehmen/Orte.txt","Orte");
			readInsertFile("Muellunternehmen/Tagesplan.txt","Tagesplan");
			
			GUI window = new GUI();

		} 
		catch(Exception e)
		{
			System.out.println("Exception: "+ e.getMessage());
			e.printStackTrace();
		}


	}

	public static void readStmtFile(String path)
	{
		try
		{
			br = new BufferedReader(new FileReader(path));
			String readLine;
			String line="";
			while(true)
			{
				readLine = null;
				readLine = br.readLine();
				if(readLine==null)
				{
					if(br.readLine()==null)
					{
						if(br.readLine()==null)
						{
							break;
						}
					}

				}
				else if(readLine.startsWith("--"))
				{
					readLine="";
				}

				line=line+readLine;
				if(line.contains(";"))
				{
					System.out.println(line);
					line.replaceAll("`", "");
					useStatement(line);
					line="";
				}

			}


			br.close();



		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void  useStatement(String stmtS)
	{

		try {
			stmt= c.createStatement();
			stmt.execute(stmtS);
			stmt.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void readInsertFile(String path, String table)
	{
		String checkedString = "";
		String toCheckString;
		try
		{
			br = new BufferedReader(new FileReader(path));
			String readLine;
			while(true)
			{
				readLine = null;
				readLine = br.readLine();
				if(readLine==null)
				{
					checkedString="";
					break;

				}

				System.out.println(readLine);
				checkedString="";
				line=false;
				while(readLine.contains(","))
				{
					
					if(readLine.contains(","))
					{
						toCheckString=readLine.substring(0,readLine.indexOf(","));
					}
					else
					{
						toCheckString=readLine.substring(0,readLine.length());
					}
					System.out.println(toCheckString);
					toCheckString=toCheckString.replaceAll(" ", "");
					pruefeString(toCheckString);
					if(ziffer==true)
					{
						checkedString= checkedString + String.valueOf(zahlen)+",";
					}
					else
					{
						checkedString= checkedString +"\""+ buchstaben +"\",";
					}
					readLine=readLine.substring(readLine.indexOf(",")+1,readLine.length());
					System.out.println(readLine);

				}
				pruefeString(readLine);
				if(ziffer==true)
				{
					checkedString= checkedString + String.valueOf(zahlen)+",";
				}
				else
				{
					checkedString= checkedString +"\""+ buchstaben +"\",";
				}
				System.out.println(checkedString);
				checkedString=checkedString.substring(0, checkedString.length()-1);
				System.out.println(checkedString);
				useStatement("insert into "+ table +" values("+checkedString+");");

			}


			br.close();



		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void pruefeString (String a) 
	{
		System.out.println(":"+a);
		try {

			zahlen = Integer.parseInt(a);
			ziffer = true;
			System.out.println("ist ein Integer");
		} 
		catch(NumberFormatException e)
		{
			buchstaben=a;
			ziffer = false;
			System.out.println("ist ein String");
		}

	}
	
	

}
