import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Main {

	public static Connection c = null;
	public static Statement stmt=null;
	public static ResultSet rs = null;

	public static void main(String[] args) {

		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			c = DriverManager.getConnection("jdbc:mysql://localhost?useSSL=false","test","test");
			stmt= c.createStatement();
			
			stmt.execute("drop database speedTest;");
			System.out.println("dropped database");

			stmt.execute("create database speedTest;");
			System.out.println("created database");

			stmt.execute("use speedTest;");
			System.out.println("connected to database");

			stmt.execute("create table speedTest (text VARCHAR(255))");
			System.out.println("created table");
			
			long startTime= System.currentTimeMillis();
			
			for(int i=0;i<10000;i++)
			{
				stmt.executeUpdate("insert into speedTest (text) values (\""+i+"\") ;");
			}
			
			startTime=System.currentTimeMillis()-startTime;
			System.out.println(startTime+"ms");
			
			stmt.close();
			
		} catch (Exception e) {
			 
			e.printStackTrace();
		}


		
	}
}
