

import java.util.List;

import redis.clients.jedis.Jedis;

public class Main {

	public static void main(String[] args) {
		
		
		Jedis jedis = new Jedis("localhost"); 
		System.out.println("Connection to server sucessfully"); 
		
		jedis.del("speedtest");
		
		long startTime= System.currentTimeMillis();
		
		for(int i=0;i<10000;i++)
		{
			jedis.lpush("speedtest", "TEST"); 
		}
		   
		startTime=System.currentTimeMillis()-startTime;
		
		List<String> list = jedis.lrange("speedtest", 0 , -1); 
		
		
//		for(int i = 0; i<list.size(); i++) { 
//	        System.out.println("Stored string in redis:: "+list.get(i)); 
//	    } 
		
		System.out.println(startTime+"ms");
	}

}
